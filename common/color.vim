colorscheme molokai

hi Normal ctermfg=250 ctermbg=233
hi Ignore ctermfg=244 ctermbg=none
hi Title ctermfg=208

hi diffRemoved ctermfg=1 cterm=none
hi diffAdded ctermfg=2 cterm=none
hi DiffDelete ctermfg=88 ctermbg=9
hi DiffAdd ctermfg=22 ctermbg=10
hi DiffChange ctermfg=229 ctermbg=234
hi DiffText ctermfg=22 ctermbg=10

hi Search ctermbg=11 ctermfg=0
hi LineNr ctermbg=234 ctermfg=59
hi CursorLineNr ctermbg=234 ctermfg=11
hi Folded ctermfg=59 ctermbg=233
hi Pmenu ctermfg=81 ctermbg=233
hi PmenuSel ctermfg=118 ctermbg=233

hi scssSelectorName ctermfg=81
hi link scssImport Keyword
hi link scssVariable Keyword
hi link scssProperty cssProp

hi MatchParen ctermbg=240 ctermfg=15 cterm=none
hi ColorColumn ctermbg=234 cterm=none
hi SpellBad ctermbg=1 ctermfg=15 cterm=underline
hi SpellCap ctermbg=166 ctermfg=15 cterm=underline
hi Comment cterm=italic
hi ErrorMsg ctermbg=1 ctermfg=15
hi link jsonCommentError Comment
hi clear Error80
hi clear cssBraceError
hi clear jsonTrailingCommaError
hi clear jsParensError
