nnoremap <buffer> gj :call js#OpenJSFile()<CR>
nnoremap <buffer> gt :call js#OpenTestFile()<CR>
nnoremap <buffer> gc :call js#OpenScssFile()<CR>
nnoremap <buffer> gs :call js#OpenSnapshotFile()<CR>
