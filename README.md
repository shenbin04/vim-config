# Installation
--------------

To install:
```sh
git clone https://github.com/shenbin04/vim-config.git ~/.vim
cd ~/.vim && ./install.sh
```
